#
# Copyright 2012 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


# Inherit some common liquid stuff.
$(call inherit-product, vendor/liquid/config/common_phone.mk)

# Inherit from hardware-specific part of the product configuration
$(call inherit-product, device/lge/geeb/device.mk)

# Inherit from common hardware-specific part of the product configuration
$(call inherit-product, device/lge/gee-common/gee-common.mk)

PRODUCT_NAME := liquid_geeb
PRODUCT_DEVICE := geeb
PRODUCT_BRAND := LGE
PRODUCT_MODEL := Optimus G
PRODUCT_MANUFACTURER := LGE

# Kernel inline build
TARGET_KERNEL_CONFIG := geeb_defconfig
TARGET_VARIANT_CONFIG := geeb_defconfig
TARGET_SELINUX_CONFIG := geeb_defconfig

$(call inherit-product, vendor/lge/gee/gee-vendor.mk)

# Boot animation
TARGET_SCREEN_WIDTH := 768
TARGET_SCREEN_HEIGHT := 1280
